#ifndef FILE_OPS
#define FILE_OPS

#include <stdio.h>
#include "image.h"
#include "status.h"
#include "bmp.h"

enum read_status read_image(char* const filename, struct image* const img);

enum write_status write_image(char* const filename, struct image* const img);

#endif
