#include "image.h"
#include  <stdint.h>

struct image sepia_c(struct image source);
struct image sepia_asm(struct image source);
