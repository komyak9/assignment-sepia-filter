#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "image.h"
#include "status.h"
#pragma pack(push, 1)

#define TYPE 19778
#define PLANES 1
#define DIB_SIZE 40
#define BPP 24
#define COMP 0
#define X_PPM 0
#define Y_PPM 0
#define IMP_COLORS 0
#define NUM_COLORS 0
#define HEADER_SIZE 54

#ifndef BMP
#define BMP
struct bmp_header{
    uint16_t bfType;            
    uint32_t bFileSize;       
    uint32_t bfReserved;        
    uint32_t bOffBits;          
    uint32_t biSize;        
    uint32_t biWidth;            
    uint32_t biHeight;         
    uint16_t biPlanes;          
    uint16_t biBitCount;            
    uint32_t biCompression;   
    uint32_t biSizeImage;      
    uint32_t biXPelsPerMeter;          
    uint32_t biYPelsPerMeter;           
    uint32_t biClrUsed;        
    uint32_t  biClrImportant;
};
#endif

enum read_status from_bmp( FILE* in, struct image* img );
enum write_status to_bmp( FILE* out, struct image const* img );
#pragma pack(pop)
