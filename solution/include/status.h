#ifndef STATUS
#define STATUS

enum read_status  {
  READ_OK = 0,
  READ_FILE_OK, 
  READ_HEADER_ERR,
  READ_FILE_ERR
};

enum  write_status  {
  WRITE_OK = 0,
  WRITE_FILE_OK,
  WRITE_ERR,
  WRITE_FILE_ERR
};

#endif
