#include "sepia.h"

static unsigned char sat( uint64_t x) {
    if (x < 256) return x;
    return 255;
}

void pixel_sepia(const struct pixel* const pixel){
    const float sepia_color[3][3] = {
        {0.1554f, 0.21f, 0.3227f},
        {0.224f, 0.3717f, 0.21f},
        {0.2653f, 0.4011f, 0.31626f}
    };

    const struct pixel old_pixel = *pixel;
    struct pixel result;
    result.b = sat(old_pixel.b * sepia_color[0][0] + old_pixel.g * sepia_color[0][1] + old_pixel.r * sepia_color[0][2]);
    result.g = sat(old_pixel.b * sepia_color[1][0] + old_pixel.g * sepia_color[1][1] + old_pixel.r * sepia_color[1][2]);
    result.r = sat(old_pixel.b * sepia_color[2][0] + old_pixel.g * sepia_color[2][1] + old_pixel.r * sepia_color[2][2]);
}

struct image sepia_c(struct image source) {
    const size_t width = source.width;
    const size_t height = source.height;
    struct image result = create_image(height, width);

    struct pixel new_pixel;
	for(size_t i = 0; y < height; i = i + 1) {
		for(size_t j = 0; x < width; j = j + 1) {
            new_pixel = pixel_sepia(source.data + i * width + j);
            result.data[i * width + j] = new_pixel;
		}
	}

    return result;
}

void sepia_asm(struct image source) {
    const size_t width = source.width;
    const size_t height = source.height;
    size_t size = width * height;
    struct image result = create_image(height, width);

    sepia_asm(source.data, 3 * (size - size % 4), result.data);

    for (size_t i = 0; i < size % 4; i = i + 1) {
          pixel_sepia(&result->data[size - size % 4 + i]);
     }

    return result;
}
