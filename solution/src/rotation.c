#include "rotation.h"

struct image rotate(struct image source) {
  uint64_t size = source.height * source.width;
  uint64_t x;
  uint64_t y;

  struct pixel* rotated_pixels = malloc(sizeof(struct pixel) * size);
  struct pixel* original_pixels = source.data;

  struct image result = create_image(source.height, source.width);
  for(size_t i = 0; i < size; i = i + 1) {
    x = source.height - i / source.width - 1;
    y = i % source.width;    
    rotated_pixels[source.height * y + x] = original_pixels[i];
  }
  
  result.data = rotated_pixels;
  return result;
}
