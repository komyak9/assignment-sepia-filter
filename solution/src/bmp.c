#include "bmp.h"

static uint32_t calc_padding(uint32_t width);

static enum read_status read_data(FILE* in, struct image img) {
  struct pixel* pixels = img.data;
    size_t width = img.width;
  size_t height = img.height;

  uint32_t padding = calc_padding(img.width);
  for (size_t i = 0; i < height; i = i + 1) {
    if (fread(pixels + width * i, sizeof(struct pixel), width, in) != width) {
      free(pixels);
      return READ_FILE_ERR;
    }
    if (fseek(in, padding, SEEK_CUR)) {
      free(pixels);
      return READ_FILE_ERR;
    }  
  }
  return READ_OK;
}

static enum write_status write_data(FILE* out, struct image const img) {
  struct pixel* new_pixel = img.data;
  size_t width = img.width;
  size_t height = img.height;

  uint32_t padding = calc_padding(img.width);
  for (size_t i = 0; i < height; i = i + 1) {
    if (fwrite(new_pixel, sizeof(struct pixel), width, out) != width) return WRITE_ERR;
    if (fwrite(new_pixel, 1, padding, out) != padding) return WRITE_ERR;
    new_pixel = new_pixel + (size_t) width;
  }
  return WRITE_OK;
}

static enum read_status read_header(FILE* in, struct bmp_header* header) {
  if (fread(header, sizeof(struct bmp_header), 1, in)) return READ_OK;
  else return READ_HEADER_ERR;
}

static enum write_status write_header(FILE* out, struct image const* img) {
  uint32_t padding = calc_padding(img->width);
  size_t size = sizeof(struct pixel) * img->width * img->height + padding * img->height;

  struct bmp_header header = (struct bmp_header) {0};
  
  header.bfType = TYPE;
  header.bFileSize = HEADER_SIZE + size;
  header.bOffBits = HEADER_SIZE;
  header.biSize = DIB_SIZE;
  header.biWidth = img->width;
  header.biHeight = img->height;
  header.biPlanes = PLANES;
  header.biBitCount = BPP;
  header.biCompression = COMP;
  header.biSizeImage = size;
  header.biXPelsPerMeter = X_PPM;
  header.biYPelsPerMeter = Y_PPM;
  header.biClrUsed = NUM_COLORS;
  header.biClrImportant = IMP_COLORS;

  if (!(fwrite(&header, sizeof(struct bmp_header), 1, out))) return WRITE_ERR;
  else return WRITE_OK;
}

static uint32_t calc_padding(uint32_t width) {
  uint32_t padding;
  if (!(width % 4)) padding = 0;
  else padding = (width * 3/4 + 1) * 4 - width * 3;
  return padding;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
  if (write_header(out, img)) return WRITE_ERR;
  return write_data(out, *img);
}

enum read_status from_bmp( FILE* in, struct image* img ){
  struct bmp_header* header = malloc(sizeof(struct bmp_header));
  enum read_status status = read_header(in, header);

  if (status) return status;

  img->data = malloc(header->biWidth * header->biHeight * sizeof(struct pixel));
  img->width = header->biWidth;
  img->height = header->biHeight;

  status = read_data(in, *img);
  free(header);

  return status;
}
