#include "file_actions.h"

enum read_status read_image(char* const filename, struct image* const img) {
  FILE *fPtr;
  fPtr = fopen(filename,"rb");
  if (!fPtr) return READ_FILE_ERR;

  enum read_status r_status = from_bmp(fPtr, img);
  if (r_status) return r_status;

  fclose(fPtr);
  return READ_FILE_OK;
}

enum write_status write_image(char* const filename, struct image* const img){
  FILE *fPtr;
  fPtr = fopen(filename,"wb");
  if (!fPtr) return WRITE_FILE_ERR;

  enum write_status w_status = to_bmp(fPtr, img);
  if(w_status) return w_status;

  fclose(fPtr);
  return WRITE_FILE_OK;
}
