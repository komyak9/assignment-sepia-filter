#include "file_actions.h"
#include "image.h"
#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>

const char* rd_status[] = {
  [READ_OK] = "Данные успешно считаны.\n",
  [READ_HEADER_ERR] = "ОШИБКА: не удалось прочитать header.\n",
  [READ_FILE_OK] = "Файл успешно считан.\n",
  [READ_FILE_ERR] = "ОШИБКА: не удалось прочесть файл.\n"
};  

const char* wr_status[] = {
  [WRITE_OK] = "Данные успешно записаны.\n",
  [WRITE_ERR] = "ОШИБКА: данные не были записаны.\n",
  [WRITE_FILE_ERR] = "ОШИБКА: данные в файл не были записаны.\n",
  [WRITE_FILE_OK] = "Данные успешно записаны в файл.\n"
};

void write_to_stdout(const char* status) {
  fprintf(stdout, "%s", status);
}

void write_to_stderr(const char* status) {
  fprintf(stderr, "%s", status);
}

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 4) {
      fprintf(stderr, "%s", "Некорректный ввод аргументов. Проверьте ввод. Программа завершается.");
      return 0;
    }

    struct image input_image = {0};
    enum read_status r_status = read_image(argv[1], &source);

    if (r_status > 1) {
      write_to_stderr(rd_status[r_status]);
      return 0;
    } else write_to_stdout(rd_status[r_status]);

    struct image transformed_image_c = {0};
    struct image transformed_image_asm = {0};
    struct rusage r;
    struct timeval start;
    struct timeval end;
    const long time_c;
    const long time_asm;

    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;
    // if argv[3] == "asm" then transformed_image_asm = sepia_asm(input_image)
    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;
    time_asm = end.tv_usec - start.tv_usec + (1000000L * (end.tv_sec - start.tv_sec));

    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;
    // if argv[3] == c then transformed_image_c = sepia_c(input_image)
    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;
    time_c = end.tv_usec - start.tv_usec + (1000000L * (end.tv_sec - start.tv_sec));

    // write_image
    // if (status)...
    // delete_image

    return 0;
}
